import ProjectDescription

let workspace = Workspace(
    name: "mssngr",
    projects: [
        "App",
        "Features/**",
    ],
    additionalFiles: [
        "Documentation/**",
        "README.md"
    ],
    generationOptions: .options(
        enableAutomaticXcodeSchemes: false,
        renderMarkdownReadme: true
    )
)
