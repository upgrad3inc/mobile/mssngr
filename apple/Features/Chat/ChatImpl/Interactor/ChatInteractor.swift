// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import Foundation
import ChatInterface

final class ChatInteractor {

    // MARK: - Properties
    
    weak var controller: (any ChatControllerProtocol)?

}

// MARK: - ChatInteractorProtocol

extension ChatInteractor: ChatInteractorProtocol {
    func viewDidLoad() { }
}
