// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import UIKit

final class ChatView: UIView {
    
    // MARK: - Properties
    
    // MARK: - Init
    
    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    func viewDidLoad() { }
}
