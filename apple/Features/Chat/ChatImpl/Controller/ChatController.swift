// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import UIKit
import ChatInterface
import MssngrUIKit

final class ChatController: UIViewController, ViewSpecifiable {
    
    // MARK: - Typealiases
    
    typealias Content = ChatView
    
    // MARK: - Properties
    
    private let interactor: ChatInteractorProtocol
    
    // MARK: - Init
    
    init(interactor: ChatInteractorProtocol) {
        self.interactor = interactor
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        view = ChatView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        content.viewDidLoad()
        interactor.viewDidLoad()
    }
}

// MARK: - ChatControllerProtocol

extension ChatController: ChatControllerProtocol {
    
}
