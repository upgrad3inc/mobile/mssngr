// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import UIKit
import MessageFeedInterface
import MssngrUIKit

final class MessageFeedController: UIViewController, ViewSpecifiable {
    
    // MARK: - Typealiases
    
    typealias Content = MessageFeedView
    
    // MARK: - Properties
    
    private let interactor: MessageFeedInteractorProtocol
    
    // MARK: - Init
    
    init(interactor: MessageFeedInteractorProtocol) {
        self.interactor = interactor
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        view = MessageFeedView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        content.viewDidLoad()
        interactor.viewDidLoad()
    }
}

// MARK: - MessageFeedControllerProtocol

extension MessageFeedController: MessageFeedControllerProtocol {
    
}
