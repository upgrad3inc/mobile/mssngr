// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import UIKit

public final class MessageFeedFactory {
    public static func makeController() -> UIViewController {
        return MessageFeedController(interactor: MessageFeedInteractor())
    }
}
