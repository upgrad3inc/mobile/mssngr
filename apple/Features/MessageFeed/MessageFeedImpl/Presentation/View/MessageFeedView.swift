// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import UIKit

final class MessageFeedView: UIView {
    
    // MARK: - Properties
    
    private lazy var collectionView = makeCollectionView()
    
    // MARK: - Init
    
    init() {
        super.init(frame: .zero)
        backgroundColor = .purple
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    private func makeCollectionView() -> UICollectionView {
        return UICollectionView()
    }
    
    // MARK: - Layout
    
    func viewDidLoad() { }
}
