// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import Foundation
import MessageFeedInterface

final class MessageFeedInteractor {

    // MARK: - Properties
    
    weak var controller: (any MessageFeedControllerProtocol)?

}

// MARK: - MessageFeedInteractorProtocol

extension MessageFeedInteractor: MessageFeedInteractorProtocol {
    func viewDidLoad() { }
}
