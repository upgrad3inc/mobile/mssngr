// Template: https://github.com/cirruslabs/macos-image-templates/blob/master/templates/xcode.pkr.hcl
packer {
  required_plugins {
    tart = {
      version = ">= 1.12.0"
      source  = "github.com/cirruslabs/tart"
    }
  }
}

variable "macos_version" {
  type = string
  default = "sonoma"
}

variable "xcode_version" {
  type = string
  default = "15.3"
}

source "tart-cli" "tart" {
  vm_base_name = "ghcr.io/cirruslabs/macos-${var.macos_version}-vanilla:latest"
  vm_name      = "sonoma-xcode-${var.xcode_version}"
  cpu_count    = 4
  memory_gb    = 8
  disk_size_gb = 90
  headless     = true
  ssh_password = "admin"
  ssh_username = "admin"
  ssh_timeout  = "120s"
}

build {
  sources = ["source.tart-cli.tart"]

  // Install homebrew
  provisioner "shell" {
    inline = [
      "touch ~/.zprofile",
      "/bin/bash -c \"$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)\"",
      "echo \"export LANG=en_US.UTF-8\" >> ~/.zprofile",
      "echo 'eval \"$(/opt/homebrew/bin/brew shellenv)\"' >> ~/.zprofile",
      "echo \"export HOMEBREW_NO_AUTO_UPDATE=1\" >> ~/.zprofile",
      "echo \"export HOMEBREW_NO_INSTALL_CLEANUP=1\" >> ~/.zprofile",
      "source ~/.zprofile",
      "brew --version",
      "brew update",
      "brew upgrade",
      "brew install cmake gcc libyaml openssl@3 graphicsmagick",
      "sudo softwareupdate --install-rosetta --agree-to-license",
    ]
  }

  // Install mise
  provisioner "shell" {
    inline = [
      "source ~/.zprofile",
      "curl https://mise.run | sh",
      "echo 'export PATH=\"$HOME/.local/bin:$PATH\"' >> ~/.zprofile",
      "echo 'export PATH=\"$HOME/.local/share/mise/shims:$PATH\"' >> ~/.zprofile",
      "source ~/.zprofile",
      "mise doctor",
      "mise settings set legacy_version_file false",
      "mise install ruby@3.3.1",
    ]
  }

  // Install xcode
  provisioner "shell" {
    inline = [
      "source ~/.zprofile",
      "brew install xcodesorg/made/xcodes",
      "xcodes version",
    ]
  }

  provisioner "file" {
    sources = [pathexpand("~/XcodesCache/Xcode_${var.xcode_version}.xip")]
    destination = "/Users/admin/Downloads/"
  }

  provisioner "shell" {
    inline = [
      "source ~/.zprofile",
      "sudo xcodes install ${var.xcode_version} --experimental-unxip --path /Users/admin/Downloads/Xcode_${var.xcode_version}.xip --select --empty-trash",
      "xcodebuild -downloadPlatform iOS Simulator",
      "xcodebuild -runFirstLaunch",
    ]
  }
  
  // Check there is at least 20GB of free space and fail if not
  provisioner "shell" {
    inline = [
      "source ~/.zprofile",
      "df -h",
      "export FREE_MB=$(df -m | awk '{print $4}' | head -n 2 | tail -n 1)",
      "[[ $FREE_MB -gt 15000 ]] && echo OK || exit 1"
    ]
  }
}