// swift-tools-version: 5.10
import PackageDescription

let package = Package(
    name: "MssngrRouting",
    platforms: [
        .iOS(.v17)
    ],
    products: [
        .library(
            name: "MssngrRouting",
            targets: [
                "MssngrRouting"
            ]
        )
    ],
    targets: [
        .target(
            name: "MssngrRouting",
            path: "Sources"
        ),
        .testTarget(
            name: "MssngrRoutingTests",
            dependencies: [
                .target(name: "MssngrRouting")
            ],
            path: "Tests"
        ),
    ]
)
