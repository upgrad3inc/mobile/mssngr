// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import UIKit

public final class NavigationController: UINavigationController {
    var rootViewController: UIViewController? {
        viewControllers.first
    }
    
    func setRootViewController(_ controller: UIViewController, animated: Bool) {
        setViewControllers([controller], animated: animated)
    }
}
