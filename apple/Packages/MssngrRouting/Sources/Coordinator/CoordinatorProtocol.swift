// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import Foundation

public protocol CoordinatorProtocol: AnyObject {
    var children: [CoordinatorProtocol] { get set }
    var router: RouterProtocol { get }

    func start()
    func start(with deeplink: Deeplink)
}

// MARK: - Add/Remove

public extension CoordinatorProtocol {
    func addChild(_ coordinator: CoordinatorProtocol) {
        guard !children.contains(where: { $0 === coordinator }) else { return }
        children.append(coordinator)
    }

    func removeChild(_ coordinator: CoordinatorProtocol) {
        guard !children.isEmpty else { return }
        children.removeAll { $0 === coordinator }
    }
}
