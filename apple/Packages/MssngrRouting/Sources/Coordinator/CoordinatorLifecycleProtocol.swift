// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import Foundation

/// Интерфейс жизненного цикла координатора
public protocol LifeCycleListener: AnyObject {
    func increment()
    func decrement()
    func startNotify()
//    func dismissNotify(event: ApplicationRouter.RouterEvent)
    func toRootNofity(in router: RouterProtocol)
}
