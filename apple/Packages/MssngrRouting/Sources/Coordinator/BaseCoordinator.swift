// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import Foundation

public class BaseCoordinator: CoordinatorProtocol {
    
    // MARK: - Properties
    
    public var children: [any CoordinatorProtocol] = []
    public private(set) var router: RouterProtocol
    
    // MARK: - Init
    
    init(router: RouterProtocol) {
        self.router = router
    }
    
    // MARK: - Functions
    
    public func start() { }
    
    public func start(with deeplink: Deeplink) { }
}
