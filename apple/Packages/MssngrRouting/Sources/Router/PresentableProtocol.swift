// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import UIKit

public protocol PresentableProtocol {
    var toPresent: UIViewController? { get }
}

// MARK: - PresentableProtocol

extension UIViewController: PresentableProtocol {
    public var toPresent: UIViewController? {
        return self
    }
}
