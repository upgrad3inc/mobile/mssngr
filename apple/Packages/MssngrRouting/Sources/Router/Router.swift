// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import UIKit

public final class Router: NSObject {
    private var navigationController = NavigationController()
}

// MARK: - RouterProtocol

extension Router: RouterProtocol {
    public func setRoot(_ presentable: any PresentableProtocol) {
        
    }
    
    public func setRoot(_ presentable: any PresentableProtocol, transition: (any TransitionProtocol)?) {
        
    }
    
    public func push(_ presentable: any PresentableProtocol) {
        
    }
    
    public func push(_ presentable: any PresentableProtocol, animated: Bool) {
        
    }
    
    public func push(_ presentable: any PresentableProtocol, animated: Bool, transition: (any TransitionProtocol)?) {
        
    }
    
    public func present(_ presentable: any PresentableProtocol) {
        print(presentable)
    }
    
    public func present(_ presentable: any PresentableProtocol, animated: Bool) {
        print("\(presentable) animated: \(animated)")
    }
}

// MARK: - UINavigationControllerDelegate

extension Router: UINavigationControllerDelegate {
    public func navigationController(
        _ navigationController: UINavigationController,
        willShow viewController: UIViewController,
        animated: Bool
    ) {
        
    }
    
    public func navigationController(
        _ navigationController: UINavigationController,
        didShow viewController: UIViewController,
        animated: Bool
    ) {
        
    }
    
    public func navigationController(
        _ navigationController: UINavigationController,
        interactionControllerFor animationController: any UIViewControllerAnimatedTransitioning
    ) -> (any UIViewControllerInteractiveTransitioning)? {
        return nil
    }
    
    public func navigationController(
        _ navigationController: UINavigationController,
        animationControllerFor operation: UINavigationController.Operation,
        from fromVC: UIViewController,
        to toVC: UIViewController
    ) -> (any UIViewControllerAnimatedTransitioning)? {
        return nil
    }
}

// MARK: - UIAdaptivePresentationControllerDelegate

extension Router: UIAdaptivePresentationControllerDelegate {
    public func presentationControllerWillDismiss(_ presentationController: UIPresentationController) {
        
    }
    
    public func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        
    }
    
    public func presentationController(
        _ presentationController: UIPresentationController,
        willPresentWithAdaptiveStyle style: UIModalPresentationStyle,
        transitionCoordinator: (any UIViewControllerTransitionCoordinator)?
    ) {
        
    }
}
