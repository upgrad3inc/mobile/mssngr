// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import UIKit

public protocol TransitionProtocol: AnyObject {
    var transitioning: UIViewControllerAnimatedTransitioning { get }
}
