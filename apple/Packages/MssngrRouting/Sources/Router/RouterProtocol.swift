// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import Foundation

/// Интерфейс роутера для системы координаторов
public protocol RouterProtocol: AnyObject {
    func setRoot(_ presentable: PresentableProtocol)
    func setRoot(_ presentable: PresentableProtocol, transition: TransitionProtocol?)
    
    func push(_ presentable: PresentableProtocol)
    func push(_ presentable: PresentableProtocol, animated: Bool)
    func push(_ presentable: PresentableProtocol, animated: Bool, transition: TransitionProtocol?)
    
    func present(_ presentable: PresentableProtocol)
    func present(_ presentable: PresentableProtocol, animated: Bool)
}
