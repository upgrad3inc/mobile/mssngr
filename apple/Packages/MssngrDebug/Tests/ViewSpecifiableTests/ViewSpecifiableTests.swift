import XCTest
@testable import MssngrUIKit

final class ViewSpecifiableTests: XCTestCase {
    func testValid() {
        // Arrange
        let validEmail = "EmailValidator"
        let sut = "EmailValidator"

        // Act
        let result = sut.contains(validEmail)

        // Assert
        XCTAssertTrue(result)
    }
    
    func testInvalid() {
        // Arrange
        let invalidEmail = "Invalid"
        let sut = "EmailValidator"

        // Act
        let result = sut.contains(invalidEmail)

        // Assert
        XCTAssertFalse(result)
    }
}
