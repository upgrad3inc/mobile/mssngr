// swift-tools-version: 5.10
import PackageDescription

let package = Package(
    name: "MssngrDebug",
    platforms: [
        .iOS(.v17)
    ],
    products: [
        .library(
            name: "MssngrDebug",
            targets: [
                "MssngrDebug"
            ]
        )
    ],
    targets: [
        .target(
            name: "MssngrDebug",
            path: "Sources"
        ),
        .testTarget(
            name: "MssngrDebugTests",
            dependencies: [
                .target(name: "MssngrDebug")
            ],
            path: "Tests"
        ),
    ]
)
