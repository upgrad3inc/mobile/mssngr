import UIKit

public protocol ViewSpecifiable {
    associatedtype Content: UIView
    var content: Content { get }
}

public extension ViewSpecifiable where Self: UIViewController {
    var content: Content {
        guard let content = view as? Content else { fatalError("The view is not specified as \(Content.self)") }
        return content
    }
}
