// swift-tools-version: 5.10
import PackageDescription

let package = Package(
    name: "MssngrUIKit",
    platforms: [
        .iOS(.v17)
    ],
    products: [
        .library(
            name: "MssngrUIKit",
            targets: [
                "MssngrUIKit"
            ]
        )
    ],
    targets: [
        .target(
            name: "MssngrUIKit",
            path: "Sources"
        ),
        .testTarget(
            name: "MssngrUIKitTests",
            dependencies: [
                .target(name: "MssngrUIKit")
            ],
            path: "Tests"
        ),
    ]
)
