// swift-tools-version: 5.10
import PackageDescription

let package = Package(
    name: "MssngrBox",
    platforms: [
        .iOS(.v17)
    ],
    products: [
        .library(
            name: "MssngrBox",
            targets: [
                "MssngrBox"
            ]
        )
    ],
    targets: [
        .target(
            name: "MssngrBox",
            path: "Sources"
        ),
        .testTarget(
            name: "MssngrBoxTests",
            dependencies: [
                .target(name: "MssngrBox")
            ],
            path: "Tests"
        ),
    ]
)
