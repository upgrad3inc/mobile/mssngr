// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import Foundation

final class MessagesRepository {
    
    // MARK: - Properties
    
    private let storage: MessagesStorageProtocol
    
    // MARK: - Init
    
    init(storage: MessagesStorageProtocol) {
        self.storage = storage
    }
}

extension MessagesRepository: MessagesRepositoryProtocol {
    
}
