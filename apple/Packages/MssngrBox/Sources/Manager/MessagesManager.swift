// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import Foundation

final class MessagesManager {
    
    // MARK: - Properties
    
    private let repository: MessagesRepositoryProtocol
    
    private var observations = [ObjectIdentifier: Observation]()
    
    // MARK: - Init
    
    init(repository: MessagesRepositoryProtocol) {
        self.repository = repository
    }
}

// MARK: - MessagesManagerProtocol

extension MessagesManager: MessagesManagerProtocol {
    func addObserver(_ observer: MessagesObserverProtocol) {
        let id = ObjectIdentifier(observer)
        observations[id] = Observation(observer: observer)
    }

    func removeObserver(_ observer: MessagesObserverProtocol) {
        let id = ObjectIdentifier(observer)
        observations.removeValue(forKey: id)
    }
}

// MARK: - Observation

extension MessagesManager {
    struct Observation {
        weak var observer: MessagesObserverProtocol?
    }
}
