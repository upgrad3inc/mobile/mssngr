// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import Foundation

public protocol MessagesManagerProtocol {
    func addObserver(_ observer: MessagesObserverProtocol)
    func removeObserver(_ observer: MessagesObserverProtocol)
}
