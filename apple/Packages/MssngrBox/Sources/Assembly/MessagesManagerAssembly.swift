// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import Foundation

final class MessagesManagerAssembly {
    func assemble() -> MessagesManagerProtocol {
        let storage = MessagesStorage()
        let repository = MessagesRepository(storage: storage)
        let manager = MessagesManager(repository: repository)
        return manager
    }
}
