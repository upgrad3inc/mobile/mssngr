// Copyright (c) 2024 upgrad3 Inc. All rights reserved.

import Foundation

extension Message {
    public enum Kind {
        case text
        case image
        case video
    }
}
