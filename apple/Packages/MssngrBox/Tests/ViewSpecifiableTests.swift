import XCTest
@testable import MssngrBox

final class ViewSpecifiableTests: XCTestCase {
    func makeMarkdown() throws -> [AttributedString] {
        var result = [AttributedString]()
        let markdown = "**EmailValidator**"
        for _ in 1...10000 {
            let attributedString = try AttributedString(markdown: markdown)
            result.append(attributedString)
        }
        return result
    }
    
    func makeNotMarkdown() -> [AttributedString] {
        var result = [AttributedString]()
        let markdown = "EmailValidator"
        for _ in 1...10000 {
            let attributedString = AttributedString(markdown)
            result.append(attributedString)
        }
        return result
    }
    
    
    func testMarkdown() throws {
        self.measure {
            do {
                let tested = try makeMarkdown()
            } catch {
                print(error)
            }
            
        }
    }
    
    func testNotMarkdown() throws {
        self.measure {
            let tested = makeNotMarkdown()
        }
    }
}
