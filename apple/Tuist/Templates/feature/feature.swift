import ProjectDescription
import ProjectDescriptionHelpers

let nameAttribute: Template.Attribute = .required("name")

let template = Template(
    description: "Template for creating a feature framework",
    attributes: [
        nameAttribute
    ],
    items: [
        .file(
            path: "\(nameAttribute)/Project.swift",
            templatePath: "Project.stencil"
        ),
        .file(
            path: "\(nameAttribute)/\(nameAttribute)Example/AppDelegate.swift",
            templatePath: "Example/AppDelegate.stencil"
        ),
        .file(
            path: "\(nameAttribute)/\(nameAttribute)Interface/Controller/\(nameAttribute)ControllerProtocol.swift",
            templatePath: "Presentation/Controller/ControllerProtocol.stencil"
        ),
        .file(
            path: "\(nameAttribute)/\(nameAttribute)Interface/Interactor/\(nameAttribute)InteractorProtocol.swift",
            templatePath: "Presentation/Interactor/InteractorProtocol.stencil"
        ),
        .file(
            path: "\(nameAttribute)/\(nameAttribute)Impl/Controller/\(nameAttribute)Controller.swift",
            templatePath: "Presentation/Controller/Controller.stencil"
        ),
        .file(
            path: "\(nameAttribute)/\(nameAttribute)Impl/Interactor/\(nameAttribute)Interactor.swift",
            templatePath: "Presentation/Interactor/Interactor.stencil"
        ),
        .file(
            path: "\(nameAttribute)/\(nameAttribute)Impl/View/\(nameAttribute)View.swift",
            templatePath: "Presentation/View/View.stencil"
        ),

        .string(
            path: "\(nameAttribute)/\(nameAttribute)Testing/File.swift",
            contents: "import Foundation"
        ),
        .string(
            path: "\(nameAttribute)/\(nameAttribute)Accessibility/File.swift",
            contents: "import Foundation"
        ),
        .string(
            path: "\(nameAttribute)/\(nameAttribute)UnitTests/File.swift",
            contents: "import Foundation"
        ),
        .string(
            path: "\(nameAttribute)/\(nameAttribute)UITests/File.swift",
            contents: "import Foundation"
        ),
    ]
)
