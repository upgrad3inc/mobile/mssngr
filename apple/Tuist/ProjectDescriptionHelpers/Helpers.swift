import Foundation
import ProjectDescription

public enum Dependencies {
    case ui
    case routing
    case module(String)
    
    var name: String {
        switch self {
        case .ui: "MssngrUIKit"
        case .routing: "MssngrRouting"
        case .module(let name): name
        }
    }
}

extension Collection where Element == Dependencies {
    func makeDependencies() -> [TargetDependency] {
        return self.map { dependency in
            if case let .module(name) = dependency {
                return TargetDependency.project(target: "\(name)Interface", path: "../\(name)")
            }
            return TargetDependency.external(name: dependency.name)
        }
    }
}

public extension String {
    static func makeBundleId(for name: String) -> String {
        return "\(bundleIdPrefix).\(name)"
    }
}

public extension Project {
    // TODO: сделать зависимости на кор и на модули отдельно???
    static func feature(name: String, dependencies: Dependencies...) -> Project {
        Project(
            name: name,
            targets: [
                .target(
                    name: "\(name)Example",
                    destinations: .iOS,
                    product: .app,
                    bundleId: .makeBundleId(for: "\(name)Example"),
                    infoPlist: .default,
                    sources: "\(name)Example/**",
                    dependencies: [
                        .target(name: "\(name)Impl")
                    ]
                ),
                .target(
                    name: "\(name)Interface",
                    destinations: .iOS,
                    product: .framework,
                    bundleId: .makeBundleId(for: "\(name)Interface"),
                    infoPlist: .default,
                    sources: "\(name)Interface/**",
                    dependencies: [
                        .external(name: Dependencies.routing.name)
                    ]
                ),
                .target(
                    name: "\(name)Impl",
                    destinations: .iOS,
                    product: .framework,
                    bundleId: .makeBundleId(for: "\(name)Impl"),
                    infoPlist: .default,
                    sources: "\(name)Impl/**",
                    dependencies: [
                        .target(name: "\(name)Interface"),
                        .target(name: "\(name)Accessibility")
                    ] + dependencies.makeDependencies()
                ),
                .target(
                    name: "\(name)Testing",
                    destinations: .iOS,
                    product: .framework,
                    bundleId: .makeBundleId(for: "\(name)Testing"),
                    infoPlist: .default,
                    sources: "\(name)Testing/**",
                    dependencies: [
                        .target(name: "\(name)Interface"),
                    ]
                ),
                .target(
                    name: "\(name)Accessibility",
                    destinations: .iOS,
                    product: .framework,
                    bundleId: .makeBundleId(for: "\(name)Accessibility"),
                    infoPlist: .default,
                    sources: "\(name)Accessibility/**"
                ),
                .target(
                    name: "\(name)UnitTests",
                    destinations: .iOS,
                    product: .unitTests,
                    bundleId: .makeBundleId(for: "\(name)UnitTests"),
                    infoPlist: .default,
                    sources: "\(name)UnitTests/**",
                    dependencies: [
                        .target(name: "\(name)Interface"),
                        .target(name: "\(name)Impl"),
                        .target(name: "\(name)Testing")
                    ]
                ),
                .target(
                    name: "\(name)UITests",
                    destinations: .iOS,
                    product: .uiTests,
                    bundleId: .makeBundleId(for: "\(name)UITests"),
                    infoPlist: .default,
                    sources: "\(name)UITests/**",
                    dependencies: [
                        .target(name: "\(name)Example"),
                        .target(name: "\(name)Accessibility")
                    ]
                )
            ],
            fileHeaderTemplate: .string("Copyright (c) \(Date().formatted(Date.FormatStyle().year(.defaultDigits))) upgrad3 Inc. All rights reserved.")
        )
    }
}
