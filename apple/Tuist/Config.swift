import ProjectDescription

let config = Config(
    compatibleXcodeVersions: .upToNextMajor("15.2"),
    swiftVersion: "5.9",
    generationOptions: .options(
        staticSideEffectsWarningTargets: .all,
        enforceExplicitDependencies: true
    )
)
