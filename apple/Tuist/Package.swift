// swift-tools-version: 5.10
import PackageDescription

#if TUIST
    import ProjectDescription
    import ProjectDescriptionHelpers

    let packageSettings = PackageSettings(
        productTypes: [
            "MssngrUIKit": .framework,
            "MssngrRouting": .framework,
        ]
    )
#endif

let package = Package(
    name: "Dependencies",
    dependencies: [
        .package(path: "../Packages/MssngrUIKit"),
        .package(path: "../Packages/MssngrRouting")
    ]
)
