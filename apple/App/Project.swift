import ProjectDescription
import ProjectDescriptionHelpers

let project = Project(
    name: "App",
    organizationName: "ru.upgrad3",
    targets: [
        .target(
            name: "App",
            destinations: .iOS,
            product: .app,
            bundleId: .makeBundleId(for: "mssngr"),
            deploymentTargets: .iOS("17.0"),
            infoPlist: .default,
            sources: [
                .glob("Sources/**")
            ],
            resources: [
                .glob(pattern: "Resources/**")
            ]
        ),
        .target(
            name: "AppTests",
            destinations: .iOS,
            product: .unitTests,
            bundleId: .makeBundleId(for: "mssngrTests"),
            infoPlist: .default,
            sources: "Tests/**",
            dependencies: [
                .xctest,
                .target(name: "App")
            ]
        )
    ]
)
